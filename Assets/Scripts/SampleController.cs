﻿using System;
using UnityEditor;
using UnityEngine;

public class SampleController : MonoBehaviour
{

    public delegate void HandleEvent();
    [ShowEvent]
    public event HandleEvent TestAction;

    [DisplayInInspector]
    public void TestMethod()
    {

    }

}

[AttributeUsage(AttributeTargets.Event)]
public class ShowEventAttribute : PropertyAttribute { }

[CustomEditor(typeof(MonoBehaviour), true)]
public class ShowEventDrawer : Editor
{

    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();

        Debug.Log("Hi Event");

        GUILayout.Label("Hi Event");

    }

}

[AttributeUsage(AttributeTargets.Method)]
public class DisplayInInspectorAttribute : PropertyAttribute { }

[CustomEditor(typeof(MonoBehaviour), true)]
public class DisplayInInspectorDrawer : Editor
{

    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();

        Debug.Log("Hi Method");

        GUILayout.Label("Hi Method");

    }

}
