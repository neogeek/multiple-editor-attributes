# Multiple Editor Attributes

## Problem

This repo has been created to document and understand how to correctly setup multiple editor attributes like the ones in the sample below. Currently, the code used to create these attributes conflict with each other, and in turn make it impossible for further editor attributes from being created.

## Sample Code

### Usage

```csharp
public class SampleController : MonoBehaviour
{

    public delegate void HandleEvent();
    [ShowEvent]
    public event HandleEvent TestAction;

    [DisplayInInspector]
    public void TestMethod()
    {

    }

}
```

### Custom Editor Attributes

```csharp
[AttributeUsage(AttributeTargets.Event)]
public class ShowEventAttribute : PropertyAttribute { }

[CustomEditor(typeof(MonoBehaviour), true)]
public class ShowEventDrawer : Editor
{

    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();

        Debug.Log("Hi Event");

        GUILayout.Label("Hi Event");

    }

}

[AttributeUsage(AttributeTargets.Method)]
public class DisplayInInspectorAttribute : PropertyAttribute { }

[CustomEditor(typeof(MonoBehaviour), true)]
public class DisplayInInspectorDrawer : Editor
{

    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();

        Debug.Log("Hi Method");

        GUILayout.Label("Hi Method");

    }

}
```

## Screenshots

### Inspector

![](Screenshots/inspector.png)

### Console

![](Screenshots/console.png)
